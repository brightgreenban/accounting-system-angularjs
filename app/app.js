var shopApp = angular.module('shopApp', ['ngRoute']);

shopApp.config(function($routeProvider) {

	$routeProvider

	.when('/', {
		templateUrl: 'views/auth.html',
		controller: 'authCtrl'
	})
	.when('/home', {
		templateUrl: 'views/home.html',
		controller: 'homeCtrl'
	})
	.when('/manage', {
		templateUrl: 'views/manage.html',
		controller: 'manageCtrl'
	})
	.when('/order', {
		templateUrl: 'views/order.html',
		controller: 'orderCtrl'
	})
	.when('/message', {
		templateUrl: 'views/message.html',
		controller: 'messageCtrl'
	})
	.when('/settings', {
		templateUrl: 'views/settings.html',
		controller: 'settingsCtrl'
	}) .otherwise({
		redirectTo: '/'
	});
});

/* FACTORY FOR SIDE MENU
shopApp.factory('sideMenuService', function() {
	return {
		getSideMenuData: function() {
			sideMenuData = [
			{href: "#!home" , icon: "../app/images/nav-home.png"},
			{href: "#!order" , icon: "../app/images/nav-order.png"},
			{href: "#!manage" , icon: "../app/images/nav-edit.png"},
			{href: "#!message" , icon: "../app/images/nav-mess.png"},
			{href: "#!settings" , icon: "../app/images/nav-settings.png"},
			{href: "#!auth" , icon: "../app/images/nav-logout.png"},
			];
		}
	};
});
*/

shopApp.controller('authCtrl', ['$scope', '$http', function($scope, $http){
	$scope.links = [
	{href: "#" , text: "Информация о нашей компании"},
	{href: "#" , text: "Пользовательское соглашение"},
	{href: "#" , text: "Полезные ссылки"},
	];

	//auth by the tokens
	$scope.postData = function() {
		$http({
			url: 'https://localhost:5001/api/Auth',
			method: "POST",
			data: { "login" : $scope.email, "password": $scope.password }
		})
		.then(function(response) {
			let refreshToken = response.data.refreshToken;
			let accessToken = response.data.accessToken;

			tokenStorage = localStorage;

			localStorage.setItem("accessToken", accessToken);
			localStorage.setItem("refreshToken", refreshToken);

			if (localStorage.getItem("accessToken") && localStorage.getItem("refreshToken")) {
				window.location.href = '/#!home';
			} else {
				localStorage.removeItem("accessToken");
				localStorage.removeItem("refreshToken");

				window.location.href = '/#';
			}

			console.log(response);

			console.log(localStorage.getItem("accessToken"), localStorage.getItem("refreshToken"));

		}, 
		function(response) {
    	//response.data - вывести ошибку на проверку данных
    	console.log(response);
    });
	}

}]);

shopApp.controller('homeCtrl', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope){
	//get user email for header section
	$scope.getUserInfo = function () {
		let token = localStorage.getItem("accessToken");
		let decode = jwt_decode(token);
		$rootScope.userSub = decode.sub;
		console.log(decode.sub);
		$http({
			url: "https://localhost:5001/api/User/" + decode.sub
		}) .then(function (response) {
			let userEmail = response.data.email;
			localStorage.setItem("userEmail", userEmail);
			$rootScope.getUserEmail = localStorage.getItem("userEmail");

		}),
		function (response){
			console.log("Error");
		}
	}
	
	//get data for table
	$scope.getAccountInfo = function () {
		$http({
			url: "https://localhost:5001/api/Account"
		}) .then(function (response) {

			$scope.accountArr = response.data.accounts;

			//get profit
			let count = 0;
			let sum = 0;

			$scope.accountArr.forEach(function(element) {
				if (!element.status) {
					count++;
					sum += element.price;
				}
			});

			$scope.count = count;
			$scope.sum = sum;

			console.log($scope.accountArr);
		})
	}

	$scope.createOrder = function (id){
		console.log(id);
		$http({
			url: "https://localhost:5001/api/Account/" + id
		}) .then(function (response){
			let accountId = response.data.id;
			localStorage.setItem("id", accountId);
			$rootScope.accountId = localStorage.getItem("id");
			console.log(response + ":" + accountId);
			window.location.href = '#!/order';
		}),
		function (response) {
			console.log("Error");
		}
	}

	//init fuction
	$scope.init = function() {
		$scope.getAccountInfo();
		$scope.getUserInfo();
		console.log($scope.getUserEmail);
	}

	const MAX_TABLE_ROWS = 6;


}]);

shopApp.controller('manageCtrl', ['$scope', '$http', function($scope, $http){
	
}]);

shopApp.controller('orderCtrl', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope){
	//get order by id
	$scope.getOrderById = function (id){
		console.log(id);
		$http({
			url: "https://localhost:5001/api/Account/" + id
		}) .then(function (response){
			
			$scope.mailAccount = {
				login: response.data.mailAccount.login,
				password: response.data.mailAccount.password,
				secret: response.data.mailAccount.secret
			};

			$scope.instAccount = {
				login: response.data.instAccount.login,
				password: response.data.instAccount.password
			};
		}),
		function (response) {
			console.log("Error");
		}
	}

	$scope.getOrderById(localStorage.getItem("id"));


	//init fuction
	$scope.init = function() {
		$scope.getOrderById($rootScope.accountId);
		console.log($rootScope.accountId);
	}
}]);

shopApp.controller('messageCtrl', ['$scope', function($scope){

}]);

shopApp.controller('settingsCtrl', ['$scope', function($scope){

}]);